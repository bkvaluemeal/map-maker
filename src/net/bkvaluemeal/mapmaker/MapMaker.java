package net.bkvaluemeal.mapmaker;

import net.bkvaluemeal.mapmaker.gui.Frame;

/**
 * Map Maker
 * 
 * A Java application for creating visual tile maps
 * 
 * @author bkvaluemeal
 */
public class MapMaker
{
	public static final Frame INSTANCE = new Frame();

	public static void main(String[] args)
	{
		System.out.println("Map Maker v0.3.0\n");
	}
}
