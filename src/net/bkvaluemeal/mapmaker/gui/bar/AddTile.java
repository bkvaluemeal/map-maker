package net.bkvaluemeal.mapmaker.gui.bar;

import javax.swing.JButton;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.AddTileListener;

/**
 * The add tile button
 * 
 * @author bkvaluemeal
 */
public class AddTile extends JButton
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new AddTile
	 */
	public AddTile()
	{
		this.setToolTipText("Add Tile");
		this.setIcon(Icon.ADD.getIcon());
		this.addActionListener(new AddTileListener());
	}
}
