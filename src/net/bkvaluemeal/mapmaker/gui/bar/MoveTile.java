package net.bkvaluemeal.mapmaker.gui.bar;

import javax.swing.JButton;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.MoveTileListener;

/**
 * The move tile button
 * 
 * @author bkvaluemeal
 */
public class MoveTile extends JButton
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new MoveTile
	 */
	public MoveTile()
	{
		this.setToolTipText("Move Tile");
		this.setIcon(Icon.MOVE.getIcon());
		this.addActionListener(new MoveTileListener());
	}
}
