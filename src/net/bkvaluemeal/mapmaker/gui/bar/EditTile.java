package net.bkvaluemeal.mapmaker.gui.bar;

import javax.swing.JButton;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.EditTileListener;

/**
 * The edit tile button
 * 
 * @author bkvaluemeal
 */
public class EditTile extends JButton
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new EditTile
	 */
	public EditTile()
	{
		this.setToolTipText("Edit Tile");
		this.setIcon(Icon.EDIT.getIcon());
		this.addActionListener(new EditTileListener());
	}
}
