package net.bkvaluemeal.mapmaker.gui.bar;

import javax.swing.JButton;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.RemoveTileListener;

/**
 * The remove tile button
 * 
 * @author bkvaluemeal
 */
public class RemoveTile extends JButton
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new RemoveTile
	 */
	public RemoveTile()
	{
		this.setToolTipText("Remove Tile");
		this.setIcon(Icon.REMOVE.getIcon());
		this.addActionListener(new RemoveTileListener());
	}
}
