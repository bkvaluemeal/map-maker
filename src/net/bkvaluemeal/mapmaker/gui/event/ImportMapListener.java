package net.bkvaluemeal.mapmaker.gui.event;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import net.bkvaluemeal.mapmaker.MapMaker;

/**
 * The ActionListener for importing a map
 * 
 * @author bkvaluemeal
 */
public class ImportMapListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e)
	{
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 0));

		JTextArea text = new JTextArea(20, 40);
		text.setEditable(true);

		panel.add(text);

		JOptionPane.showMessageDialog(MapMaker.INSTANCE, panel, "Import Map", JOptionPane.PLAIN_MESSAGE);

		MapMaker.INSTANCE.setStatus("Imported map");
	}
}
