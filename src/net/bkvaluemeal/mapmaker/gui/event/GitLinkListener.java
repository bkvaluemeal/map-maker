package net.bkvaluemeal.mapmaker.gui.event;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;

/**
 * The ActionListener for the source JMenuItem
 * 
 * @author bkvaluemeal
 */
public class GitLinkListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e)
	{
		try
		{
			Desktop.getDesktop().browse(new URI("https://dev.bkvaluemeal.net/map-maker.git"));
		}
		catch (Exception ex)
		{

		}
	}
}
