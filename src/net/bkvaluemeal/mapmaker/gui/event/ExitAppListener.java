package net.bkvaluemeal.mapmaker.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JOptionPane;

import net.bkvaluemeal.mapmaker.MapMaker;

/**
 * The WindowAdapter and ActionListener for exiting the application
 * 
 * @author bkvaluemeal
 */
public class ExitAppListener extends WindowAdapter implements ActionListener
{
	private boolean exitApp()
	{
		return JOptionPane.showConfirmDialog(MapMaker.INSTANCE, "Are you sure?", "Exit", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (exitApp())
		{
			System.exit(0);
		}
	}

	@Override
	public void windowClosing(WindowEvent e)
	{
		if (exitApp())
		{
			System.exit(0);
		}
	}
}
