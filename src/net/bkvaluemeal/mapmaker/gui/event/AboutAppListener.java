package net.bkvaluemeal.mapmaker.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import net.bkvaluemeal.mapmaker.MapMaker;
import net.bkvaluemeal.mapmaker.gui.Icon;

/**
 * The ActionListener for the about JMenuItem
 * 
 * @author bkvaluemeal
 */
public class AboutAppListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e)
	{
		// @formatter:off
		JOptionPane.showConfirmDialog
		(
			MapMaker.INSTANCE,
			"MapMaker v0.3.0\n"
			+ "\n"
			+ "A Java application for creating visual tile maps.",
			"About",
			JOptionPane.DEFAULT_OPTION,
			JOptionPane.PLAIN_MESSAGE,
			Icon.LOGO.getIcon()
		);
		// @formatter:on
	}
}
