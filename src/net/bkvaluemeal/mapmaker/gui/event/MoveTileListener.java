package net.bkvaluemeal.mapmaker.gui.event;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.bkvaluemeal.mapmaker.MapMaker;

/**
 * The ActionListener for moving tiles
 * 
 * @author bkvaluemeal
 */
public class MoveTileListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e)
	{
		JTextField x1 = new JTextField(4);
		JTextField y1 = new JTextField(4);
		JTextField x2 = new JTextField(4);
		JTextField y2 = new JTextField(4);

		JPanel fromPanel = new JPanel();
		fromPanel.add(new JLabel("From"));
		fromPanel.add(new JLabel("X:"));
		fromPanel.add(x1);
		fromPanel.add(new JLabel("Y:"));
		fromPanel.add(y1);

		JPanel toPanel = new JPanel();
		toPanel.add(new JLabel("To    "));
		toPanel.add(new JLabel("X:"));
		toPanel.add(x2);
		toPanel.add(new JLabel("Y:"));
		toPanel.add(y2);

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2, 2));
		panel.add(fromPanel);
		panel.add(toPanel);

		if (JOptionPane.showConfirmDialog(MapMaker.INSTANCE, panel, "Move Tile", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
		{
			MapMaker.INSTANCE.setStatus("Moved tile (" + x1.getText() + ", " + y1.getText() + ") to (" + x2.getText() + ", " + y2.getText() + ")");
		}
	}
}
