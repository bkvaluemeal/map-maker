package net.bkvaluemeal.mapmaker.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import net.bkvaluemeal.mapmaker.MapMaker;

/**
 * The ActionListener for creating a new tile map
 * 
 * @author bkvaluemeal
 */
public class NewTileMapListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (JOptionPane.showConfirmDialog(MapMaker.INSTANCE, "Are you sure?", "New", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION)
		{
			MapMaker.INSTANCE.setStatus("New tile map");
		}
	}
}
