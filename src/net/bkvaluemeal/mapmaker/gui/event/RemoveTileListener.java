package net.bkvaluemeal.mapmaker.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.bkvaluemeal.mapmaker.MapMaker;

/**
 * The ActionListener for removing a tile
 * 
 * @author bkvaluemeal
 */
public class RemoveTileListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e)
	{
		JTextField x = new JTextField(4);
		JTextField y = new JTextField(4);

		JPanel panel = new JPanel();
		panel.add(new JLabel("X:"));
		panel.add(x);
		panel.add(new JLabel("Y:"));
		panel.add(y);

		if (JOptionPane.showConfirmDialog(MapMaker.INSTANCE, panel, "Remove Tile", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION)
		{
			MapMaker.INSTANCE.setStatus("Removed tile (" + x.getText() + ", " + y.getText() + ")");
		}
	}
}
