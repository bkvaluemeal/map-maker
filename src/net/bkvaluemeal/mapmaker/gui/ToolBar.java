package net.bkvaluemeal.mapmaker.gui;

import javax.swing.JToolBar;

import net.bkvaluemeal.mapmaker.gui.bar.AddTile;
import net.bkvaluemeal.mapmaker.gui.bar.EditTile;
import net.bkvaluemeal.mapmaker.gui.bar.MoveTile;
import net.bkvaluemeal.mapmaker.gui.bar.RemoveTile;

/**
 * The JToolBar for the application
 * 
 * @author bkvaluemeal
 */
public class ToolBar extends JToolBar
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new ToolBar
	 */
	public ToolBar()
	{
		super(JToolBar.VERTICAL);
		this.setFloatable(false);
		this.add(new AddTile());
		this.add(new RemoveTile());
		this.add(new EditTile());
		this.add(new MoveTile());
	}
}
