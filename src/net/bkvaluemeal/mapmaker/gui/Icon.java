package net.bkvaluemeal.mapmaker.gui;

import java.awt.Image;

import javax.swing.ImageIcon;

import net.bkvaluemeal.mapmaker.MapMaker;

/**
 * The enum of ImageIcons for the application
 * 
 * @author bkvaluemeal
 */
public enum Icon
{
	// @formatter:off
	ADD("add.png"),
	EDIT("edit.png"),
	EXPORT("export.png"),
	EXIT("exit.png"),
	ICON("icon_16.png"),
	LOGO("icon_64.png"),
	IMPORT("import.png"),
	ISC("isc.png"),
	MOVE("move.png"),
	NEW("new.png"),
	OSI("osi.png"),
	REMOVE("remove.png"),
	SOURCE("source.png");
	// @formatter:on

	private ImageIcon image;

	/**
	 * Creates a new Icon
	 * 
	 * @param filePath the file path
	 */
	Icon(String filePath)
	{
		this.image = new ImageIcon(MapMaker.class.getResource("res/" + filePath));
	}

	/**
	 * Gets the Icon
	 * 
	 * @return the Icon
	 */
	public ImageIcon getIcon()
	{
		return this.image;
	}

	/**
	 * Gets this Icon's image
	 * 
	 * @return the Image
	 */
	public Image getImage()
	{
		return this.image.getImage();
	}
}
