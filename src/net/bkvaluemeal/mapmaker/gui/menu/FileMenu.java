package net.bkvaluemeal.mapmaker.gui.menu;

import javax.swing.JMenu;

import net.bkvaluemeal.mapmaker.gui.menu.item.ExitApp;
import net.bkvaluemeal.mapmaker.gui.menu.item.ExportMap;
import net.bkvaluemeal.mapmaker.gui.menu.item.ImportMap;
import net.bkvaluemeal.mapmaker.gui.menu.item.NewMap;

/**
 * The file menu for the application
 * 
 * @author bkvaluemeal
 */
public class FileMenu extends JMenu
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new FileMenu
	 */
	public FileMenu()
	{
		super("File");
		this.add(new NewMap());
		this.add(new ImportMap());
		this.add(new ExportMap());
		this.addSeparator();
		this.add(new ExitApp());
	}
}
