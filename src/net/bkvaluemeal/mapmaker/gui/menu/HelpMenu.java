package net.bkvaluemeal.mapmaker.gui.menu;

import javax.swing.JMenu;

import net.bkvaluemeal.mapmaker.gui.menu.item.AboutApp;
import net.bkvaluemeal.mapmaker.gui.menu.item.AppLicense;
import net.bkvaluemeal.mapmaker.gui.menu.item.GitLink;

/**
 * The help menu for the application
 * 
 * @author bkvaluemeal
 */
public class HelpMenu extends JMenu
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new HelpMenu
	 */
	public HelpMenu()
	{
		super("Help");
		this.add(new AboutApp());
		this.add(new AppLicense());
		this.add(new GitLink());
	}
}
