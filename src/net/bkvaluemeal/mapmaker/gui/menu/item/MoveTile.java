package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.MoveTileListener;

/**
 * The move tile menu item
 * 
 * @author bkvaluemeal
 */
public class MoveTile extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new MoveTile
	 */
	public MoveTile()
	{
		super("Move Tile");
		this.setIcon(Icon.MOVE.getIcon());
		this.addActionListener(new MoveTileListener());
	}
}
