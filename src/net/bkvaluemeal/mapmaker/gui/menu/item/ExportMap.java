package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.ExportMapListener;

/**
 * The export map menu item
 * 
 * @author bkvaluemeal
 */
public class ExportMap extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new ExportMap
	 */
	public ExportMap()
	{
		super("Export");
		this.setIcon(Icon.EXPORT.getIcon());
		this.addActionListener(new ExportMapListener());
	}
}
