package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.AddTileListener;

/**
 * The add tile menu item
 * 
 * @author bkvaluemeal
 */
public class AddTile extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new AddTile
	 */
	public AddTile()
	{
		super("Add Tile");
		this.setIcon(Icon.ADD.getIcon());
		this.addActionListener(new AddTileListener());
	}
}
