package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.AboutAppListener;

/**
 * The about menu item
 * 
 * @author bkvaluemeal
 */
public class AboutApp extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new AboutApp
	 */
	public AboutApp()
	{
		super("About");
		this.setIcon(Icon.ICON.getIcon());
		this.addActionListener(new AboutAppListener());
	}
}
