package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.EditTileListener;

/**
 * The edit tile menu item
 * 
 * @author bkvaluemeal
 */
public class EditTile extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new EditTile
	 */
	public EditTile()
	{
		super("Edit Tile");
		this.setIcon(Icon.EDIT.getIcon());
		this.addActionListener(new EditTileListener());
	}
}
