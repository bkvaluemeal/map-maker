package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.ExitAppListener;

/**
 * The exit menu item
 * 
 * @author bkvaluemeal
 */
public class ExitApp extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new ExitApp
	 */
	public ExitApp()
	{
		super("Exit");
		this.setIcon(Icon.EXIT.getIcon());
		this.addActionListener(new ExitAppListener());
	}
}
