package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.GitLinkListener;

/**
 * The source code menu item
 * 
 * @author bkvaluemeal
 */
public class GitLink extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new GitLink
	 */
	public GitLink()
	{
		super("Source");
		this.setIcon(Icon.SOURCE.getIcon());
		this.addActionListener(new GitLinkListener());
	}
}
