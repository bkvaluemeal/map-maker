package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.ImportMapListener;

/**
 * The import map menu item
 * 
 * @author bkvaluemeal
 */
public class ImportMap extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new ImportMap
	 */
	public ImportMap()
	{
		super("Import");
		this.setIcon(Icon.IMPORT.getIcon());
		this.addActionListener(new ImportMapListener());
	}
}
