package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.AppLicenseListener;

/**
 * The license menu item
 * 
 * @author bkvaluemeal
 */
public class AppLicense extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new AppLicense
	 */
	public AppLicense()
	{
		super("License");
		this.setIcon(Icon.OSI.getIcon());
		this.addActionListener(new AppLicenseListener());
	}
}
