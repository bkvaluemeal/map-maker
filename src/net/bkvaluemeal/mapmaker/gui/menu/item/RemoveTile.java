package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.RemoveTileListener;

/**
 * The remove tile menu item
 * 
 * @author bkvaluemeal
 */
public class RemoveTile extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new RemoveTile
	 */
	public RemoveTile()
	{
		super("Remove Tile");
		this.setIcon(Icon.REMOVE.getIcon());
		this.addActionListener(new RemoveTileListener());
	}
}
