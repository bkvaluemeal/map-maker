package net.bkvaluemeal.mapmaker.gui.menu.item;

import javax.swing.JMenuItem;

import net.bkvaluemeal.mapmaker.gui.Icon;
import net.bkvaluemeal.mapmaker.gui.event.NewTileMapListener;

/**
 * The new map menu item
 * 
 * @author bkvaluemeal
 */
public class NewMap extends JMenuItem
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a NewMap
	 */
	public NewMap()
	{
		super("New");
		this.setIcon(Icon.NEW.getIcon());
		this.addActionListener(new NewTileMapListener());
	}
}
