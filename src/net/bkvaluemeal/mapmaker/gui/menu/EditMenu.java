package net.bkvaluemeal.mapmaker.gui.menu;

import javax.swing.JMenu;

import net.bkvaluemeal.mapmaker.gui.menu.item.AddTile;
import net.bkvaluemeal.mapmaker.gui.menu.item.EditTile;
import net.bkvaluemeal.mapmaker.gui.menu.item.MoveTile;
import net.bkvaluemeal.mapmaker.gui.menu.item.RemoveTile;

/**
 * The edit menu for the application
 * 
 * @author bkvaluemeal
 */
public class EditMenu extends JMenu
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new EditMenu
	 */
	public EditMenu()
	{
		super("Edit");
		this.add(new AddTile());
		this.add(new RemoveTile());
		this.add(new EditTile());
		this.add(new MoveTile());
	}
}
