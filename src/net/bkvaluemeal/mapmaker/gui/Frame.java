package net.bkvaluemeal.mapmaker.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import net.bkvaluemeal.mapmaker.gui.event.ExitAppListener;

/**
 * The JFrame for the application
 * 
 * @author bkvaluemeal
 */
public class Frame extends JFrame
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new instance of the application
	 */
	public Frame()
	{
		this.setTitle("Map Maker");
		this.setIconImage(Icon.LOGO.getImage());
		this.setSize(1280, 720);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new ExitAppListener());

		this.setJMenuBar(new MenuBar());
		this.add(new ToolBar(), BorderLayout.WEST);
		this.add(new Map(), BorderLayout.CENTER);
		this.add(new StatusBar(), BorderLayout.SOUTH);

		this.setVisible(true);
	}

	/**
	 * Sets the status message
	 * 
	 * @param status the status message
	 */
	public void setStatus(String status)
	{
		((StatusBar) this.getContentPane().getComponent(2)).setText(status);
		System.out.println(status);
	}

	/**
	 * Gets the map JPanel
	 * 
	 * @return the map
	 */
	public Map getMap()
	{
		return (Map) this.getContentPane().getComponent(1);
	}
}
