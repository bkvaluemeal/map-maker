package net.bkvaluemeal.mapmaker.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * The JPanel map for the application
 * 
 * @author bkvaluemeal
 */
public class Map extends JPanel
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new Map
	 */
	public Map()
	{
		this.setBackground(Color.BLACK);
	}

	/**
	 * Paints the components to the map
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	}
}
