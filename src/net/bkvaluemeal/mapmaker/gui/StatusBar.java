package net.bkvaluemeal.mapmaker.gui;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;

/**
 * The JLabel status bar for the application
 * 
 * @author bkvaluemeal
 */
public class StatusBar extends JLabel
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new StatusBar
	 */
	public StatusBar()
	{
		super(" New tile map");
		this.setPreferredSize(new Dimension(-1, 22));
		this.setBorder(LineBorder.createGrayLineBorder());
	}

	@Override
	public void setText(String status)
	{
		super.setText(" " + status);
	}
}
