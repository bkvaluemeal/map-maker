package net.bkvaluemeal.mapmaker.gui;

import javax.swing.Box;
import javax.swing.JMenuBar;

import net.bkvaluemeal.mapmaker.gui.menu.EditMenu;
import net.bkvaluemeal.mapmaker.gui.menu.FileMenu;
import net.bkvaluemeal.mapmaker.gui.menu.HelpMenu;

/**
 * The JMenuBar for the application
 * 
 * @author bkvaluemeal
 */
public class MenuBar extends JMenuBar
{
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new MenuBar
	 */
	public MenuBar()
	{
		this.add(new FileMenu());
		this.add(new EditMenu());
		this.add(Box.createHorizontalGlue());
		this.add(new HelpMenu());
	}
}
