# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 0.3.0 - Events
### Added
- ActionListeners

### Changed
- Main to MapMaker

## 0.2.0 - Icons
### Added
- Icons

## 0.1.0 - Simple GUI
### Added
- GUI
- Menus
- Tool bar
- Empty map
- Status text
